function Person(name,age){
    this.name = name;
    this.age = age;
    this.run = function(){
        console.log('run')
    }
}

Person.prototype.eat = function(){
    console.log('eat')
}
Person.prototype.say = function(){
    console.log('say')
}

//借用构造函数实现继承  对象冒充 缺点：只能继承父类的私有属性和方法。原型上的继承不到
// function Student(name,age){
//     Person.call(this,name,age);
// }

// var stu1 = new Student('小花',18);
// var stu2 = new Student('小hei',20);
// stu1.eat();
// console.log(stu1)

//原型链继承 缺点：子类传的参数，父类接受不到
// function Student(name,age){
   
// }
// Student.prototype = new Person();

// var stu1 = new Student('小花',20);
// console.log(stu1)


//组合继承：
// function Student(name,age){
//     Person.call(this,name,age);
// }
// Student.prototype = new Person();
// var stu1 = new Student('小花',20);
// console.log(stu1)


//原型继承
function Student(name,age){
    Person.call(this,name,age);
}
//寄生组合继承
Student.prototype = Object.create(Person.prototype)
Student.prototype.aa = 123;
var stu1 = new Student('小花',20);
console.log(Person.prototype)