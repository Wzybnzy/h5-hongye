// function func(name) {
//     console.log(name);
//     var name = 25;
//     console.log(name);
//     function name() {}
//     console.log(name);
// }

// func(18);

(function (a) {
    console.log(typeof a());
})(function () {
    return 1;
});

function Foo() {
    getName = function () {
        alert(1);
    };
    return this;
}
Foo.getName = function () {
    alert(2);
};
Foo.prototype.getName = function () {
    alert(3);
};
var getName = function () {
    alert(4);
};
function getName() {
    alert(5);
}

Foo.getName();
getName();
Foo().getName();
getName();
new Foo.getName();
new Foo().getName();
new new Foo().getName();
