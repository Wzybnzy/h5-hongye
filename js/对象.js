//1.对象字面量  new Object()
// var obj = {
//     name:'小花',
//     age:19,
//     eat:function(){
//         console.log('eat')
//     },
//     run:function(){
//         console.log('run')
//     }
// }

// var obj1 = {
//     name:'小黑',
//     age:20,
//     eat:function(){
//         console.log('eat')
//     },
//     run:function(){
//         console.log('run')
//     }
// }

//2. 工厂模式出 缺点：1. 不知道当前对象的类型 2.里面的方法重复的定义
// function person(name,age){
//     var p = {};
//     p.name = name;
//     p.age = age;
//     p.eat = function(){
//         console.log('eat')
//     }
//     p.run = function(){
//         console.log('eat')
//     }
//     return p;
// }


// var p1 = person('小花',18);
// var p2 = person('小黑',20);

// console.log(p1)
// console.log(p2)


// 3. 构造函数
// function Person(name,age){
//     this.name = name;
//     this.age = age;
//     this.eat= function(){
//         console.log('eat')
//     }
//     this.run = function(){
//         console.log('run')
//     }
// }

// var p1 = new Person('小花',18);
// var p2 = new Person('小黑',20);
// console.log(p1)
// console.log(p2)


//4。 原型 
//每一个对象上边都有一个[[prototype]]属性。这个属性被称为原型。(隐士原型)
//ECAMscript没有规范，来访问这个属性
//浏览器制定了一个__proto__这个属性，来访问原型
// var obj = {name:'1902A'};

// {name:'',__proto__:{}}
// console.log(obj.__proto__.__proto__)

// obj.__proto__.age = 20;
//当对象访问属性的时候，会触发对象的get函数。先在当前对象上边查找属性。差不多，会沿着原型链向上查找。
// console.log(obj.age)

//函数 prototype 每一个函数上边都有一个prototype的属性。这个属性是(显示属性)
//还是一个对象 __proto__  

// function foo(){

// }
// console.dir(foo)



// function Person(){

// }
// Person.prototype.name = 'xiaohua';
// Person.prototype.age = '19';
// Person.prototype.eat = function(){
//     console.log('eat')
// }
// Person.prototype.run = function(){
//     console.log('eat')
// }

// var p1 = new Person();
// var p2 = new Person();

// console.log(Person.prototype) 

//1.创建一个对象
// 2. obj.__proto__ = Person.prototype;
// 3. 把对象复制this
// 4. 执行代码
// 5. 返回这个对象

function Person(){ //prototype
//    var obj = {};
//    obj.__proto__=Person.prototype;
//    this = obj;
//    执行代码
//    return obj;
}
var p1 = new Person();
var p2 = new Person();

console.log(Person.prototype)