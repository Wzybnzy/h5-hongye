function Person(name,age){
    this.name = name;
    this.age = age;
}

// Person.prototype.eat = function(){
//     console.log('eat')
// }
// Person.prototype.say = function(){
//     console.log('say')
// }


Person.prototype = {
    constructor:Person,
    eat:function(){
        console.log('eat')
    },
    say:function(){
        console.log('say')
    }
}

let p1= new Person('小花',19);
let p2= new Person('小黑',20);
console.log(p1)
p1.say();
p2.eat();