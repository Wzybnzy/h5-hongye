// pages/list/list.js
import {getList} from '../../api/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:"小花",
    page:1,
    pageSize:10,
    list:[],
    loading:true,
    search:''
  },
  handleTap:function(e){
    console.log(e);
    this.setData({
      name:'6666'
    })
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  getInit:async function(){
    let res = await getList({
      keyword:12,
      page:this.data.page,
      pageSize:10,
    });
    this.setData({
      list:this.data.list.concat(res.list2),
      loading:false
    })
    console.log(res,'*****12312');
  },
  onLoad: function (options) {
    console.log(options,'*****');
    this.getInit();
    this.setData({
      search:options.val
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('onPullDownRefresh');
    this.setData({
      page:1,
      list:[]
    })
    this.getInit();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('onReachBottom');
    if(this.data.page < 5){
      this.setData({
        page:this.data.page+1
      })
      this.getInit();
    }
   
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})