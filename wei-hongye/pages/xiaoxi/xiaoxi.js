// pages/xiaoxi/xiaoxi.js
let timer = null;
let count = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  sendSearchOrder:function(){
    wx.cloud.callFunction({
      name:'payment_res'
    }).then(res =>{
      //不开启定时器
      clearTimeout(timer);
      wx.navigateTo({
        url: '/pages/result',
      })
    }).catch(err=>{
      //轮询
        this.SearchOrder();
    })
  },
  SearchOrder:function(){
    count++;
    if(count>5){
      wx.showToast({
        title: '支付异常，联系人工',
      })
      return;
    }
    clearTimeout(timer);
    timer = setTimeout(()=>{
      this.sendSearchOrder();
    },1000)
  },
  handlePay:function(){ //相当于后台接口
    wx.cloud.callFunction({
      name:"payment"
    }).then(res=>{
      console.log(res,'******')
      const payment = res.result.payment;
      wx.requestPayment({
        ...payment,
        success:(data)=>{
          console.log(data);
          count = 1;
          // //轮询查询订单状态
          this.sendSearchOrder();
        },
        fail:function(err){
          console.log(err);
        }
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})