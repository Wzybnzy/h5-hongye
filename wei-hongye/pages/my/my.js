const { httpTools } = require("../../utils/http");

// pages/my/my.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  handleLogin:function(){
    wx.getUserProfile({ //用户授权
      desc: '用于完善个人信息资料',
      success:function(res){
        console.log(res);
        wx.login({ //通过这wx.login() 拿code凭证，然后把code发给后台的登录接口。通过code拿到opendid,秘钥。生成一个token，返回给前端。前端把token存储到本地。
          success:function(data){
            console.log(data,'*****');
            // httpTools({
            //   url:'/login',
            //   data:{
            //     code:data.code
            //   }
            // })
          }
        })
      },
      fail:function(err){
        console.log(err,'****');
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})