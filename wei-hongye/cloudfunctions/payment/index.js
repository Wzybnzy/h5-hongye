// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

function getOutTradeNo(len) {
  let $chars =
        'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678' 
  let maxPos = $chars.length
  let pwd = ''
  for (let i = 0; i < len; i++) {
    pwd += $chars.charAt(Math.floor(Math.random() * maxPos))
  }
  return pwd
};
 
// 云函数入口函数
exports.main = async (event, context) => {
  // 下单接口
  const outTradeNo = getOutTradeNo(32);
  const res = await cloud.cloudPay.unifiedOrder({
    "body" : "红叶找房付费咨询",
    outTradeNo,
    "spbillCreateIp" : "127.0.0.1",
    "subMchId" : "1599958401",
    "totalFee" : 1,
    "envId": "min-gucof",
    "functionName": "payment_cb"
  })
  return {...res,outTradeNo}
}