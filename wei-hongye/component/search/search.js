// component/search/search.js
const {debounce} = require('../../utils/util');
import {searchCommunity} from '../../api/api'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    searchValue:{
      type:String,
      value:'默认值'
    },
    disabled:{
      type:Boolean,
      value:false
    },
    text:{
      type:String,
      value:'地图'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    value:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleConfirm:function(){
      console.log('完成',this.data.searchValue);
      let list = wx.getStorageSync('searchList');
      if(list){
        list.push({
          id:new Date().getTime(),
          val:this.data.searchValue
        })
      } else {
        list= [{
          id:new Date().getTime(),
          val:this.data.searchValue
        }]
      }

      //存储
      wx.setStorageSync('searchList',list);
      wx.navigateTo({
        url: '/pages/list/list?val='+this.data.searchValue,
      })
    },
    handleInput:debounce(async function({detail}){
      console.log(this.data.text);
      let that  = this;
      let res = await searchCommunity({
        keywords:detail,
        type:1
      });
      console.log(res,'&&&&&&&');
      this.triggerEvent('myInput',res.list); 

      // wx.request({
      //   url: 'http://yapi.smart-xwork.cn/mock/109104/wapi/index/searchCommunity.html',
      //   data:{
      //     keywords:1,
      //     type:1
      //   },
      //   method:'get',
      //   success:function(res){
      //     console.log(res)
      //     that.triggerEvent('myInput',res.data.list); //通知父组件
      //   },
      //   fail:function(err){
      //     console.log(err);
      //   }
      // })
    },300)
  }
})
