const httpTools = ({url,method,data,header})=>{
  return new Promise((resolve,reject)=>{
    const baseUrl = 'http://yapi.smart-xwork.cn/mock/109104';
    wx.showLoading({
      title: '请求加载中...',
    })
    wx.request({
      url:baseUrl+url,
      method:method || 'GET',
      header:header || {},
      data,
      success:function(res){
        if(res.data.code == 1){
          wx.hideLoading();
          resolve(res.data);
        } else if(res.data.code == 401){
          wx.showToast({
            title: '没有权限',
            duration:3000
          })
        }
      },
      fail:function(err){
        wx.showToast({
          title: '请求失败',
          duration:3000
        })
        reject(err)
      }
    })
  })
}

module.exports = {
  httpTools
}