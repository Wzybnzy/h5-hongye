const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    mode: 'development',
    devtool:'source-map',
    entry: {
        index: path.join(__dirname, './src/index.js'),
        common_index: path.join(__dirname, './src/common_index.js'),
    },
    output: {
        path: path.join(__dirname, 'build'),
        filename: '[name].js',
    },
   
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './index.html',
        }),
    ],
   
};