var __webpack_modules__ = {
  // key:()=>{}
    './src/js/format.js': module => {
        const sum = (...arg) => {
            return arg.reduce((prev, cur) => prev + cur, 0);
        };

        module.exports = {
            sum,
        };
    },
};

var __webpack_module_cache__ = {}; //缓存对象

// __webpack_module_cache__={
//   'a.js':{exports:{}},
//   'b:js':{}
// }

function __webpack_require__(moduleId) { // ./src/js/format.js
    var cachedModule = __webpack_module_cache__[moduleId]; // __webpack_module_cache__['./src/js/format.js']
    if (cachedModule !== undefined) { 
        return cachedModule.exports;
    }
    var module = (__webpack_module_cache__[moduleId] = {
        exports: {},
    });
    // module ={exports:{}}

    __webpack_modules__[moduleId](module, module.exports, __webpack_require__);

    return module.exports;
}

var __webpack_exports__ = {};

(() => {
    const { sum } = __webpack_require__('./src/js/format.js');
    console.log(sum(1, 2, 3, 4));
    // requirejs  AMD
    // node（require） Commonjs规范,只能用在服务端
    // es6Module  import export
    // webpack
})();
