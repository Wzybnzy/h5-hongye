class Vue {
    constructor(options) {
        this.$options = options;
        this.$data = options.data;
        this.$el = options.el;
        if (this.$el) {
            console.log('**************');
            this.observe(this.$data);
            new Watcher(this.$data,'name');
            this.$data.name;
        }
    }
    observe(data) {
        if (!data || typeof data !== 'object') {
            return;
        }
        // Object.keys(data) 返回值是数组
        //   console.log( Object.entries(data));
        //    console.log(Object.keys(data));
        //    console.log(Object.values(data));
        Object.keys(data).forEach(key => {
            this.defineReactive(data,key,data[key]);
        });
    }
    defineReactive(data,key,val){ // xiaohua  18  {}
        this.observe(val);
        let dep = new Dep();
    
        Object.defineProperty(data,key,{
            get(){
                Dep.target &&  dep.addDep(Dep.target)
                return val;
            },
            set(newval){
                if(val == newval){
                    return
                }
                val = newval;
                dep.notify();
                console.log(key+'属性发生了变化')
            }
        });
    }
}

class Dep{ //相当于偶像
    constructor(){
        this.deps = []
    }
    addDep(dep){
        this.deps.push(dep);
    }
    notify(){
        this.deps.forEach(dep => dep.updater())
    }
}

class Watcher{ //观察者 相当于粉丝
    constructor(vm,key){
        Dep.target = this;
        this.$vm = vm;
        this.$key = key
    }
    updater(){
        console.log(this.$key+'有活动')
    }
}