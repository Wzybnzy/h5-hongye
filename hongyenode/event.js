// let events = new Vue(); eventbus

class Event{
    constructor(){
        this.events = {}
    }
    on(eventname,ck){
        if(this.events[eventname] instanceof Array){ //第二次或者第三次
            this.events[eventname].push(ck);
        } else { //第一次
            this.events[eventname]= [ck];
        }
    }
    emit(eventname,...arg){
        this.events[eventname] && this.events[eventname].map(event => event(...arg))
    }
}

let events = new Event();

events.on('add',(data)=>{
    console.log('第一次data',data)
});
events.on('add',(data)=>{
    console.log('第二次data',data)
});
events.emit('add','6666');