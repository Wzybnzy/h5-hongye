const Koa = require('koa');
const app = new Koa();
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const axios = require('axios');
const { createServer } = require('http');
const { networkInterfaces } = require('os');
const Mock = require('mockjs');
//获取经纬度的接口
router.get('/getLocation', async ctx => {
    const url = `https://apis.map.qq.com/ws/location/v1/ip`;
    // console.log(networkInterfaces())
    try {
        let { data } = await axios.get(url, {
            params: {
                ip: '111.206.145.41',
                key: 'OHPBZ-AKCWP-EYIDH-V4UNZ-DC4JE-GMFMS',
            },
        });
        ctx.body = {
            code: 0,
            msg: '成',
            data,
        };
    } catch (error) {
        ctx.body = {
            code: 1,
            mes: '失败',
            error,
        };
    }
});
//获取房屋信息
router.get('/gethouse', async ctx => {
    let { lat, lng } = ctx.query;
    let data = Mock.mock({
        'list|10': [
            {
                id: '@id', //模拟id
                'price|10000-20000': 10000, //房屋价格
                'build_price|100000-20000': 10000, //房屋价格
                address: '@county(true)', //模拟省市县
                title: '@ctitle(4)', //模拟中文标题
                'distance|1-2': 1,
                img: "@image(150x200','#fff')",
                lat: lat,
                lng: lng,
                'leixing|1': [1, 2, 3], //模拟状态值,0,1,2,
                name: '@ctitle',
                price_type: '均价',
                price_unit: '元/m²',
            },
        ],
    });
    data.list.map(item => {
        //39.90469&lng=116.40717
        // console.log(Number(lat).toFixed(2))
        item.lat = Number(lat).toFixed(2) + Math.random().toString().slice(2, 6);
        item.lng = Number(lng).toFixed(2) + Math.random().toString().slice(2, 6);
        return item;
    });
    ctx.body = {
        code: 0,
        msg: '成功',
        data,
    };
});

//获取用户信息
router.get('/getuser', async ctx => {
    let { code } = ctx.query;
    //https://gitee.com/oauth/token?grant_type=authorization_code&code={code}&client_id={client_id}&redirect_uri={redirect_uri}&client_secret={client_secret}
    console.log(code, 'code');
    let data = await axios.post('https://gitee.com/oauth/token', {
        grant_type: 'authorization_code',
        code,
        client_id: 'f01c77f17a804f5de219a2e774a63464a81510fef910ddc119031d971e2249d6',
        client_secret: '9e2992058f76d527e7ae834100f4254a1cb28bca2e74bca7d55c328bf8551c80',
        redirect_uri: 'http://localhost:8080/login',
    });
    let res = await axios.get('https://gitee.com/api/v5/user', {
        params: {
            access_token: data.data.access_token,
        },
    });
    ctx.body = {
        code: 0,
        datas: res.data,
    };
    console.log(res.data, 'data');
});

app.use(bodyParser());
app.use(router.routes(), router.allowedMethods());
app.listen(3000);
