const Koa = require("koa");
const { createServer } = require("http");
const { Server } = require("socket.io");

const app = new Koa();
const httpServer = createServer(app.callback());
const io = new Server(httpServer, { 
  cors: true
 });

 //存放所有用户
let userList = [];
let valList = [];
io.on("connection", (socket) => {
  // ...
  // console.log('建立连接',user.id)

  socket.on('login',(name)=>{
    console.log(name,'上线了');
    userList.push(name);
    socket.lastName = name;
    //通知其他人有人上线了
    console.log(valList.length,'valList');
    socket.emit('online',userList); //通知自己
    socket.emit('allMsg',valList); //更新消息
    socket.broadcast.emit('online',userList);
  });
  socket.on('send',(val)=>{
    console.log(val,'val内容');
    let obj = {
        name:socket.lastName,
        time:new Date().toLocaleDateString(),
        val,
    }
    valList.push(obj);
    //更新消息
    socket.emit('updateMsg',obj);
    socket.broadcast.emit('updateMsg',obj);
  });
  //下线通知
  socket.on("disconnect", (reason) => {
    // ...
  });
});

httpServer.listen(3000);