const Koa = require("koa");
const { createServer } = require("http");
const { Server } = require("socket.io");

const app = new Koa();
const httpServer = createServer(app.callback());
const io = new Server(httpServer, { 
  cors: true
 });

io.on("connection", (user) => {
  // ...
  console.log('建立连接',user.id)
  user.on('msg',(msg)=>{
    console.log(msg,'msg')
  });
  user.emit('send','服务端传过来的数据');
  user.on('input',(val)=>{
    console.log(val,'val')
    //通知除了自己之外的其他人
    user.broadcast.emit('updatemsg',val);
    user.emit('updatemsg',val);
  });
});

httpServer.listen(3000);