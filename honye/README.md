# honye

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### diff

1. vue2.0 全量对比的
2. vue3.0 新增了静态标记

### 静态提升

1. 2.0 不论元素是否参与更新，每次都会重新创建，再渲染
2. 3.0 对于不参与更新的元素，会做一个静态的提升，只会创建一次，渲染时重新使用

### 事件缓存

### 调试

1. vconsole
2. 数据线联调（可以断点调试）

- 插上数据线 （照片）
- 开启手机的开发者模式
- 在开发者选项里面选择 usb 调试
- 手机下载 googl 浏览器
- 在手机浏览器上边输入地址：http://192.168.0.107:8080/
- 电脑谷歌浏览器上 chrome://inspect/#deviceschrome://inspect/#devices
- vpn 打开

  3.whistle 安装

- 在手机里面设置代理（手动）
- 手动 主机名：ip 地址 端口 8899
- 在 whistle 里面的 rules 设置

```
http://192.168.0.107:8080/ log://{test.js}
# 利用whistle查看、修改页面的DOM结构及其样式
http://192.168.0.107:8080/ weinre://test
```

### SPA 单页面应用

单页 Web 应用（single page web application，SPA），就是只有一张 Web 页面的应用，是加载单个 HTML 页面并在用户与应用程序交互时动态更新该页面的 Web 应用程序。

### 路由模式

1. hash 模式
   url 路径里面带一个#，#后边发生变化的时候，服务器不会监听。不会重新发起一个请求。
   通过 hashchange 这个事件来监听 hash 值的变化，通过 hash 值变化，来匹配对应的路由然后加载。
2. history 模式
3. 跟真的路径看起来是一样的，通过浏览器的 history 来实现跳转。（pushstate,popstate:监听路径发生变化的，replacestae,go,back,forward）,当路径发生变化的时候，不会像浏览器发起请求。
4. 刷新当前页面的时候，会发起一个新的请求。会出现一个 404 的错误。这个需要后台配置
