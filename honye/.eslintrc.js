module.exports = {
  extends: ['react-app', 'react-app/jest', 'plugin:prettier/recommended'],
  //规则
  rules: {
    'prettier/prettier': 'warn',
    'no-console': process.env.NODE_ENV === 'development' ? 'off' : 'warn',
    'no-alert': process.env.NODE_ENV === 'development' ? 'off' : 'warn',
    'no-debugger': process.env.NODE_ENV === 'development' ? 'off' : 'warn',
    'no-unused-vars': ['warn', { vars: 'all', args: 'none' }],
  },
};
