module.exports = {
    devServer:{
        proxy:{
            '/api':{
                target:'http://yapi.smart-xwork.cn/mock/109104/',
                changeOrigin:true,
                pathRewrite:{'^/api':''}
            },
            '/app':{
                target:'http://localhost:3000',
                changeOrigin:true,
                pathRewrite:{'^/app':''}
            }
        }
    }
}