import { httpAxios } from "../utils/request";

export const getlist = ()=> httpAxios.get('/api/list');
export const getLocation = ()=> httpAxios.get('/app/getLocation');
export const gethouse = (params)=> httpAxios.get('/app/gethouse',{params});
export const getposter = (params)=> httpAxios.get('/api/wapi/build/buildDetailNew.html',{params});
export const gethome = (params)=> httpAxios.get('/api/wapi/index/customIndex.html',{params});
export const gethomeList = (params)=> httpAxios.get('/api/wapi/index/infoData.html',{params});
export const getuser = (params)=> httpAxios.get('/app/getuser',{params});