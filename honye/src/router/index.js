import {createRouter,createWebHistory,createWebHashHistory} from 'vue-router'

// console.log(import('@/views/index/index'))
// new Promise(()=>{
//     console.log(11111)
// })

let routes = [
    {
        path:'/',
        component:()=> import(/* webpackChunkName: "index" */'@/views/index/index'),
        children:[
            {
                path:'/home',
                component:()=> import(/* webpackChunkName: "home" */'@/views/index/home/home'),
            },
            {
                path:'/list',
                component:()=> import(/* webpackChunkName: "list" */'@/views/index/list/list'),
            }
        ]
    },
    {
        path:'/login',
        component:()=> import(/* webpackChunkName: "login" */'@/views/login/login')
    },
    {
        path:'/map',
        component:()=> import(/* webpackChunkName: "map" */'@/views/map/map')
    },
    {
        path:'/detail',
        component:()=> import(/* webpackChunkName: "map" */'@/views/detail/detail')
    },
    {
        path:'/poster',
        component:()=> import(/* webpackChunkName: "map" */'@/views/poster/poster')
    }
]

let router = createRouter({
    history:createWebHistory(),
    routes
})
// router.beforeEach

export default router;