import MyToast from './mytoast.vue';
import {createApp,createVNode} from 'vue'

// console.log(MyToast,'MyToast4')
let toast ;
const getInstance = ()=>{
    const conatiner = document.createElement('div');
    const vNode = createVNode(MyToast); //生成虚拟DOM
    const intance = createApp(vNode).mount(conatiner); //虚拟DOM转真是DOM,挂载到div
    console.log(intance,'intance');
    toast = intance;
    document.body.appendChild(conatiner); //把创建的DIV插入到页面里面
    return intance;
}

const myToast = (option)=>{
   let intance = getInstance();
   const defaultOptions = {
       msg:'',
       duration:1000
   }
   const options = typeof option =='string' ? {msg:option} : option;
   const opt = {...defaultOptions,...options};
    intance.msg = opt.msg; 
    intance.flag = true; 
    if(!opt.duration) return;
    setTimeout(()=>{
        intance.flag = false; 
    },opt.duration)
}
myToast.clear = ()=>{
    // let intance = getInstance();
    toast.flag = false;
    console.log('clearr')
}
// class Mypersn1{
//     static clear(){

//     }
// }
// Mypersn1.clear()

// function Mypersn(){

// }
// Mypersn.clear='aa' //静态的属性方法

// let p =new Mypersn();

// export default {
//     install(app){
//         app.config.globalProperties.$mytoast = myToast;
//     }
// }

export default app => app.config.globalProperties.$mytoast = myToast;