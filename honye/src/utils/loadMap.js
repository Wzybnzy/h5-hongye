export const loadScript =()=> {
   return new Promise((resolve,reject)=>{
    //如果标签已经存在，就不用重复创建
    if(window.TMap){
        resolve();
        return;
    }
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://map.qq.com/api/gljs?v=1.exp&key=OHPBZ-AKCWP-EYIDH-V4UNZ-DC4JE-GMFMS";
    script.onload = function(){
        resolve();
    }
    document.body.appendChild(script);
   })
}

export const DomlayInit = ()=>{
     //自定义DOM覆盖物 - 继承DOMOverlay
     function myInfoWindow(options) {
        window.TMap.DOMOverlay.call(this, options);
      }
      myInfoWindow.prototype = new window.TMap.DOMOverlay();

      // 初始化
      myInfoWindow.prototype.onInit = function (options) {
        this.position = options.position;
        this.content = options.content;
        this.id = options.id;
      };

      // 创建DOM元素，返回一个DOMElement
      myInfoWindow.prototype.createDOM = function () {
        let mydom = document.createElement('div');
        mydom.setAttribute('id', this.id); //设置id
        //设置DOM样式
        mydom.style.cssText =
          'height:30px;max-width:120px;padding:5px;background:#fff;border:#ccc solid 1px;\
                        line-height:1px;font-size:12px;position:absolute;top:0px;left:0px;';
        mydom.innerHTML = this.content;
        return mydom;
      };
      // 更新DOM元素，在地图移动/缩放后执行
      myInfoWindow.prototype.updateDOM = function () {
        if (!this.map) {
          return;
        }

        // 经纬度坐标转容器像素坐标
        let pixel = this.map.projectToContainer(this.position);

        //默认使用DOM左上角作为坐标焦点进行绘制（左上对齐）
        //如想以DOM中心点（横向&垂直居中）或其它位置为焦点，可结合this.dom.clientWidth和this.dom.clientHeight自行计算
        let left = pixel.getX() - this.dom.clientWidth / 2 + 'px'; //本例水平居中
        let top = pixel.getY() + 'px';

        //将平面坐标转为三维空间坐标
        this.dom.style.transform = `translate3d(${left}, ${top}, 0px)`;
      };

      //自定义一个更新内容的方法
      myInfoWindow.prototype.updateContent = function (content) {
        mydom.innerHTML = content;
      };
      window.myInfoWindow = myInfoWindow;
}