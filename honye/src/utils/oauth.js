import {getuser} from '@/api/api'
export const getCode = ()=>{
    const url =`https://gitee.com/oauth/authorize?client_id=f01c77f17a804f5de219a2e774a63464a81510fef910ddc119031d971e2249d6&redirect_uri=http://localhost:8080/login&response_type=code`;
    window.location.href = url;
}
// http://localhost:8080/login?code=b3556d7493871432181f7f7cf4e93e9fceb06c1baf374ca4c5e83910b5a8cb8f&age=12
export const getParams = (key)=>{
    let params = window.location.search.slice(1);
    let val ;
    params.split('&').forEach(item =>{// [age=12],[code=111]
        if(item.split('=')[0] == key){
            val = item.split('=')[1]
        }
    })
    return val
}

export const getUser =async (callback)=>{
    let code = getParams('code');
    console.log(code,'&&&&&&&');
    if(code){
        let res = await getuser({code});
        if(res.data.code == 0){
            callback(res.data.datas);
        }
        console.log(res,'res')
    }
}