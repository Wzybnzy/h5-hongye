import {createApp} from 'vue'
import App from './App.vue'
import '@/utils/setRem.js'
import httpTools from '@/utils/request.js'
import VConsole from 'vconsole'
import router from '@/router/index'
import { ShareSheet,Loading } from 'vant';
import baseLoading from '@/plugins/index'
import myToast from '@/vender/mytoast/mytoast.js'
// Vue.config.productionTip = false

if(process.env.NODE_ENV !== 'production'){
    // new VConsole();
}

console.log(httpTools,'httpTools')
let app = createApp(App);
// app.use(httpTools);
// app.use(router);
// app.mount('#app')
// createApp(App)
//全局注册自定义指令
app.directive('focus',{
    mounted(el, bindings) {
        el.focus();
        let {
          modifiers: { aa },
        } = bindings;
        if (aa) {
          el.style.border = '1px solid red';
        }
        console.log(el, 'el');
        console.log(bindings, 'bindings');
      },
      
})

app.use(httpTools)
.use(router)
.use(ShareSheet)
.use(Loading)
.use(baseLoading)
.use(myToast)
.mount('#app')
// new Vue({
//   render: h => h(App),
// }).$mount('#app')
